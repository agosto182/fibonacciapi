# FibonacciApi

## Getting started

Install dependencies:

```
$ npm install
```

Run the aplication:

```
$ npm start
```

The app will run in the port 3000.


## About the solution

The solution of this problem was elaborated by calculating "n" in a linear way. That is, the program calculates each value in the sequence until it reaches "n".

How was it optimized?
For this calculation we used an array with only 2 values (the initial ones). Then each calculation was added to the array and the first value was discarded (to leave only the two we needed). This way we optimize the memory usage and stick only to the calculation.

