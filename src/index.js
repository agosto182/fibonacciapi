const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.send('Welcome! For try the solution go to the path: /:n \nExample: /300');
})

app.get('/:n', (req, res) => {
  const { n } = req.params;

  const fibonacci = [0, 1];
  let index = 0;

  while(index < parseInt(n)) {
    const nextValue = fibonacci[0] + fibonacci[1];
    console.debug('Next value is', nextValue);
    fibonacci.push(nextValue);
    fibonacci.splice(0, 1);
    index++;
  }

  const result = fibonacci[0];
  res.send({
    "n": n,
    "result": result
  })
})

app.listen(port, () => {
  console.log(`App started on port ${port}`)
})
